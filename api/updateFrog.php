<?php

$error_json = file_get_contents('./errorCodes.json');
$all_errors = json_decode($error_json, true);

$error_codes = $all_errors[selectLanguage()];
$frog_data = loadFrogData();

// verify frog is already registered
$valid_frog = verifyFrogRegistration($frog_data);

// reach to backend for the available files
$available_files = getAvailableFiles($frog_data);

echo 'available files:' . PHP_EOL;
print_r(listOfAvailableFiles($available_files));

$latest_version = getLatestVersion($available_files);
echo 'latest version is:' . $latest_version . PHP_EOL;

$current_version = getCurrentVersion($available_files, $frog_data);
if ($current_version) {
    echo 'current version is: ' . $current_version . PHP_EOL;
} else {
    echo 'current version not found in the file system' . PHP_EOL;
}

$next_version = getNextVersion($available_files, $frog_data);
if ($next_version) {
    echo 'next version is: ' . $next_version . PHP_EOL;
} else {
    echo 'next version not found in the file system' . PHP_EOL;
}

$prev_version = getPreviousVersion($available_files, $frog_data);
if ($prev_version) {
    echo 'previous version is:' . $prev_version . PHP_EOL;
} else {
    echo 'previous version not found in the file system' . PHP_EOL;
}

echo 'searching for version 1.2.0...';
$specific_version = getSpecificVersion($available_files, '1.2.0');
if ($specific_version) {
    echo 'FOUND!' . PHP_EOL;
} else {
    echo 'NOT FOUND!' . PHP_EOL;
}

$file = $latest_version->openFile();
echo 'contents of update file: ' . $file->fread($file->getSize());

function loadFrogData() {
    return [
        'arch' => 'arm',
        'deviceType' => 'pi4',
        'id' => 'dhuf39guhuiou',
        'version' => '1.0.0'
    ];
}

function getAvailableFiles($deviceData) {
    $devicePath = devicePath($deviceData);

    $dir   = new RecursiveDirectoryIterator(__DIR__);
    $flat  = new RecursiveIteratorIterator($dir);
    $files = new RegexIterator($flat, '/\.bin$/i'); // check if there's a non-regex way to grab them bin files

    $response = [];

    foreach($files as $file) {
        if(str_contains($file->getPath(), $devicePath . '/core')) {
            // we're in the right directory
            $version = $file->getBasename('.' . $file->getExtension());
            $response[$version] = $file;
        };
    }

    return $response;
}

function listOfAvailableFiles($available_files) {
    $response = [];
    foreach($available_files as $file) {
        $response[] = $file->getBasename('.' . $file->getExtension()) . PHP_EOL;
    }
    return $response;
}

function getCurrentVersion($available_files, $deviceData) {
    foreach($available_files as $ver => $file) {
        if (version_compare($deviceData['version'], $ver) === 0) {
            return $file;
        }
    }
}

function getNextVersion($available_files, $deviceData) {
    $newer_versions = [];
    foreach($available_files as $ver => $file) {
        if (version_compare($deviceData['version'], $ver) === -1) {
            return $file;
        }
    }
    return getOldestVersion($newer_versions);
}

function getPreviousVersion($available_files, $deviceData) {
    $older_versions = [];
    foreach($available_files as $ver => $file) {
        if (version_compare($deviceData['version'], $ver) === 1) {
            return $file;
        }
    }
    return getLatestVersion($older_versions);
}

function getSpecificVersion($available_files, $target_version) {
    foreach($available_files as $ver => $file) {
        if (version_compare($target_version, $ver) === 0) {
            echo '('.$target_version.' vs '.$ver.')';
            return $file;
        }
    }
}

function devicePath($deviceData) {
    return $deviceData['arch'] . '/' . $deviceData['deviceType'];
}

function getLatestVersion($available_files) {
    krsort($available_files);
    return reset($available_files); // reset gives first value in associative arrays
}

function getOldestVersion($available_files) {
    ksort($available_files);
    return reset($available_files); // reset gives first value in associative arrays
}

function verifyFrogRegistration($frog_data) {
    // 1. send $frog_data to the registration api (assuming a verification endpoint)
    // 2. turn the response into a boolean
    return !empty($frog_data);
}

// @todo actually build the logic for this
function selectLanguage() {
    return 'en';
}
