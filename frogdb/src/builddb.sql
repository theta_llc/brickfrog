-- phpMyAdmin SQL Dump
-- version 5.1.1deb5ubuntu1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 19, 2023 at 04:17 PM
-- Server version: 8.0.33-0ubuntu0.22.04.2
-- PHP Version: 8.1.2-1ubuntu2.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rush`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` varchar(36) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(25) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `name`, `created`, `created_by`, `modified`, `modified_by`) VALUES
('5bf84ecc-e287-4a5a-ae1a-5ccaa342c1b0', 'matttests', '2023-07-12 18:24:28', 'jstewart', NULL, NULL),
('876bde29-252e-4133-b22a-fab682620a76', 'Evil, Inc', '2023-07-12 17:06:10', 'jstewart', NULL, NULL),
('e88c1675-9be1-40b0-88f2-9914a4036d0b', 'theta-llc', '2023-03-13 18:01:42', 'jstewart', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customer_groups`
--

CREATE TABLE `customer_groups` (
  `id` smallint NOT NULL,
  `name` varchar(100) NOT NULL,
  `customer_id` varchar(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `customer_groups`
--

INSERT INTO `customer_groups` (`id`, `name`, `customer_id`) VALUES
(2, 'Test Group 1', 'e88c1675-9be1-40b0-88f2-9914a4036d0b'),
(7, 'dontusethisone', '876bde29-252e-4133-b22a-fab682620a76'),
(8, 'dontusethisoneither', '876bde29-252e-4133-b22a-fab682620a76');

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

CREATE TABLE `devices` (
  `id` varchar(36) NOT NULL,
  `customer_id` varchar(36) NOT NULL,
  `mac_address` varchar(18) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `is_registered` bit(1) NOT NULL DEFAULT b'0',
  `ready_to_deploy` bit(1) NOT NULL DEFAULT b'0',
  `region_id` smallint DEFAULT NULL,
  `role_id` smallint DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(25) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` varchar(25) DEFAULT NULL,
  `registered` datetime DEFAULT NULL,
  `deployed` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `devices`
--

INSERT INTO `devices` (`id`, `customer_id`, `mac_address`, `name`, `is_registered`, `ready_to_deploy`, `region_id`, `role_id`, `created`, `created_by`, `modified`, `modified_by`, `registered`, `deployed`) VALUES
('347f976f-d8a4-44fd-92a1-0aac08baf6f6', 'e88c1675-9be1-40b0-88f2-9914a4036d0b', '80:fa:5b:65:45:b6', 'testhost', b'1', b'1', 1, 1, '2023-03-17 16:37:45', 'setupService', '2023-03-21 15:11:09', 'roleService', '2023-03-21 15:10:13', NULL),
('443616ee-ce70-4587-a330-5d12f8cca90c', '876bde29-252e-4133-b22a-fab682620a76', NULL, NULL, b'1', b'0', 1, NULL, '2023-07-17 13:55:18', 'setupService', '2023-07-17 18:42:14', 'roleService', '2023-07-17 14:08:06', NULL),
('56f9d388-1a58-412b-8eab-cc8f8df1f500', '5bf84ecc-e287-4a5a-ae1a-5ccaa342c1b0', NULL, NULL, b'1', b'0', 1, NULL, '2023-07-12 18:58:53', 'setupService', '2023-07-14 15:57:00', 'registration', '2023-07-14 15:57:00', NULL),
('78c389cf-e793-4183-9c0f-d48d0cec4df1', '876bde29-252e-4133-b22a-fab682620a76', NULL, 'jefftestfrog', b'1', b'1', 1, 1, '2023-07-12 18:57:51', 'setupService', '2023-07-17 18:45:31', 'roleService', '2023-07-12 18:57:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `group_devices`
--

CREATE TABLE `group_devices` (
  `customer_group` smallint NOT NULL,
  `device` varchar(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `group_devices`
--

INSERT INTO `group_devices` (`customer_group`, `device`) VALUES
(1, '347f976f-d8a4-44fd-92a1-0aac08baf6f6'),
(2, '78c389cf-e793-4183-9c0f-d48d0cec4df1');

-- --------------------------------------------------------

--
-- Table structure for table `region`
--

CREATE TABLE `region` (
  `id` smallint NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `region`
--

INSERT INTO `region` (`id`, `name`) VALUES
(1, 'Delaware');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` smallint NOT NULL,
  `name` varchar(100) NOT NULL,
  `created` datetime NOT NULL DEFAULT (now()),
  `created_by` varchar(25) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1, 'weather', '2023-03-13 18:01:42', 'Stewart', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` varchar(36) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `customer_id` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `password`, `customer_id`) VALUES
('2bc37b08-a32c-4660-9f7f-0831482b2b9a', 's.dude@theta-llc.com', '$2y$10$Hrwn9VEU.UNZH/XUdIJXX.TKMvw/NzMwDTgOrz1jH.nZzTSfHCccW', NULL),
('d9348c8e-135c-45ce-b00e-455ee5b6d7de', 'j.stewart@theta-llc.com', '$2y$10$g.B1QRTQQY4oyi49u/6kx.UX0cmQmmpaf1sm3XN9ZTpA.UQV7gC0.1', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_groups`
--
ALTER TABLE `customer_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_customer_groups__customer` (`customer_id`);

--
-- Indexes for table `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mac_address` (`mac_address`),
  ADD KEY `fk_devices_customer` (`customer_id`),
  ADD KEY `fk_devices_role` (`role_id`),
  ADD KEY `fk_devices_region` (`region_id`);

--
-- Indexes for table `group_devices`
--
ALTER TABLE `group_devices`
  ADD PRIMARY KEY (`customer_group`,`device`),
  ADD KEY `fk_group_devices__device` (`device`);

--
-- Indexes for table `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`,`email`),
  ADD KEY `fk_user__customer` (`customer_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer_groups`
--
ALTER TABLE `customer_groups`
  MODIFY `id` smallint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `customer_groups`
--
ALTER TABLE `customer_groups`
  ADD CONSTRAINT `fk_customer_groups__customer` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`);

--
-- Constraints for table `devices`
--
ALTER TABLE `devices`
  ADD CONSTRAINT `fk_devices_customer` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`),
  ADD CONSTRAINT `fk_devices_region` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`),
  ADD CONSTRAINT `fk_devices_role` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);

--
-- Constraints for table `group_devices`
--
ALTER TABLE `group_devices`
  ADD CONSTRAINT `fk_group_devices__customer_groups` FOREIGN KEY (`customer_group`) REFERENCES `customer_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_group_devices__device` FOREIGN KEY (`device`) REFERENCES `devices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_user__customer` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
