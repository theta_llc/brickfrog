package main

import (
    "encoding/gob"
    "fmt"
    "net"
    "time"
)


func client() {
for{
        c, err := net.Dial("tcp", "127.0.0.1:7777")
        if err != nil {
            fmt.Println(err)
            return
        }
        msg := "brickfrog"
        fmt.Println("sending", msg)
        err = gob.NewEncoder(c).Encode(msg)
        if err != nil {
           fmt.Println(err)
        }
        c.Close()
        time.Sleep(1 * time.Second)
    }
}

func main() {
    go client()
    var input string
    fmt.Scanln(&input)
}
