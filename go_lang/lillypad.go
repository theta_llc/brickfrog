package main

import (
    "encoding/gob"
    "fmt"
    "net"
)

func server(){
    ln, err := net.Listen("tcp", ":7777")
    if err != nil {
        fmt.Println(err)
             }
    for {
        c, err := ln.Accept()
        if err != nil {
            fmt.Println(err)
            continue
        }
     go handleServerConnection(c)
    }
}

func handleServerConnection(c net.Conn){
    var msg string
    err := gob.NewDecoder(c).Decode(&msg)
    if err != nil {
        fmt.Println(err)
    } else {
        fmt.Println("Received" , msg)
    }
    c.Close()
}

func main() {
    go server()
    var input string
    fmt.Scanln(&input)
}
