to build
1. install go 
2. edit frogger so that it points to the desired ip address/port (command line and config file options are TBD)
3. if needed change listen port from 7777 to the desired port in lilly pad.
4. go build frogger.go
5. go build lillypad.go

known issues:
Files must be edited before build to change ip/port
File only prints a place holder holder string since they do not send actual data yet

