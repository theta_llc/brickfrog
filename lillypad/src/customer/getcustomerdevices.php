<?php

include '../include/db.php';

$secret_key="itsasecret"; // This would need to come from the database attached to the customer

$customerId=$_GET["customerId"];
$secret=$_GET["secret"]; // sent in at register from frog

// temp for testing
// $customerId="e88c1675-9be1-40b0-88f2-9914a4036d0b";
// $secret="abcd1234";

dbconnect();

if( $secret_key == $secret){
    echo "success!" . PHP_EOL;
    echo "Getting customer's devices" . PHP_EOL;
    
    $devices = get_customer_groups($myconnect, $customerId); // error handle
    if (empty($devices)){
        echo "Whoops! Error pulling up customer groups. Please try again." . PHP_EOL; // ultimately, this should throw a 400 with  information back
    } else {
        echo "The package of devices have been delivered. Ribbit.";
    }
    return $devices;
}else{
    echo "secrets do not match!" . PHP_EOL;
};

function get_customer_groups($myconnect, $customerId){
    $sqlSelect = <<<SQL
        SELECT d.id, d.name, d.mac_address, rg.name as region, rl.name as role, d.is_registered, d.ready_to_deploy, d.deployed as deployedDate
        FROM rush.devices as d
            INNER JOIN rush.customer as c on c.id = d.customer_id
            INNER JOIN rush.role as rl on rl.id = d.role_id
            INNER JOIN rush.region as rg on rg.id = d.region_id
        WHERE c.id = '$customerId';
    SQL;
    $selectResult = mysqli_query($myconnect, $sqlSelect);
    $row = $selectResult->fetch_assoc();
    var_dump($row);
    return $row;
}
?>
