<?php

include '../include/db.php';

$secret_key="itsasecret"; // This would need to come from the database attached to the customer

$customerId=$_GET["customerId"];
$secret=$_GET["secret"]; // sent in at register from frog

// temp for testing
// $customerId="e88c1675-9be1-40b0-88f2-9914a4036d0b";
// $secret="abcd1234";

dbconnect();

if( $secret_key == $secret){
    echo "success!" . PHP_EOL;
    echo "Getting customer's device groups... groups of devices? Whatever." . PHP_EOL;
    
    $groupsInfo = get_customer_groups($myconnect, $customerId); // error handle
    if (empty($groupsInfo)){
        echo "Whoops! Error pulling up customer groups. Please try again." . PHP_EOL; // ultimately, this should throw a 400 with  information back
    } else {
        echo "The package of groups have been delivered. Ribbit.";
    }
    return $groupsInfo;
}else{
    echo "secrets do not match!" . PHP_EOL;
};

function get_customer_groups($myconnect, $customerId){
    $sqlSelect = <<<SQL
        SELECT cg.id as groupId, cg.name as groupName, d.id as deviceId, d.name as deviceName, d.mac_address as deviceMacAddr
        FROM rush.customer_groups as cg
            JOIN rush.group_devices as gd on gd.customer_group = cg.id
            JOIN rush.devices as d on gd.device = d.id
        WHERE cg.customer_id = '$customerId'
        GROUP BY cg.id, d.id;
    SQL;
    $selectResult = mysqli_query($myconnect, $sqlSelect);
    $row = $selectResult->fetch_assoc();
    var_dump($row);
    return $row;
}
?>
