<?php

include '../include/db.php';

$secretkey="itsasecret";

$_POST=json_decode(file_get_contents('php://input'),true);
$header=$_POST["header"];
$package=$_POST["data"];

// let's get authentication out of the way
$secret=$header["auth"];
if ( $secret != $secretkey ) {
    return 1;
} else {
  dbconnect();
}

$deviceid=$package["deviceid"];

$devicerole=getrole($myconnect, $deviceid);

if ( empty($devicerole) ) {
  echo "Sorry, no role set for this frog." . PHP_EOL;
} else {
  $name=$devicerole["name"];
  $id=$devicerole["id"];
  echo "Frogs role is $name (id: $id)." . PHP_EOL;
}

dbclose($myconnect);

function getrole($myconnect, $deviceid){
  $actionDatetime = date("Y-m-d H:i:s");
  $sql = <<<SQL
    SELECT r.id, r.name
      FROM role as r
           JOIN devices as d ON d.role_id = r.id
     WHERE d.id = '$deviceid';
  SQL;
  $result = mysqli_query($myconnect, $sql);
  $row = $result->fetch_assoc();
  return $row;
}

?>
