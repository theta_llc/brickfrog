<?php

include '../include/db.php';

$secret_key="itsasecret"; // This would need to come from the database attached to the customer

$_POST=json_decode(file_get_contents('php://input'),true);

$deviceid=$_POST["deviceid"];
$groupid=$_POST["groupid"];
$secret=$_POST["secret"]; // sent in at register from frog

// temp for testing
// $groupid="2";
// $deviceid="347f976f-d8a4-44fd-92a1-0aac08baf6f6";
// $secret="abcd1234";

dbconnect();

if( $secret_key == $secret){
    echo "success!" . PHP_EOL;
    echo "Adding device to group." . PHP_EOL;
    
    $groupInfo = add_device_to_group($myconnect, $groupid, $deviceid); // error handle
    if (empty($groupInfo)){
        echo "Adding the device was unsuccessful. Please try again." . PHP_EOL; // ultimately, this should throw a 400 with  information back
    } else {
        $groupName = $groupInfo['groupName'];
        $deviceName = $groupInfo['deviceName'];
	$groupId = $groupInfo['id'];
        echo "The device $deviceName was successfully added to the $groupName group (id: $groupId)." . PHP_EOL;
    }
}else{
    echo "secrets do not match!" . PHP_EOL;
};

function add_device_to_group($myconnect, $groupid, $deviceid){
    $actionDatetime = date("Y-m-d H:i:s");
    $sqlInsert = <<<SQL
        INSERT INTO group_devices(customer_group, device)
        VALUE ('$groupid', '$deviceid')
    SQL;
    mysqli_query($myconnect, $sqlInsert); // need error handling for duplicate entries. 

    $sqlSelect = <<<SQL
        SELECT cg.id, cg.name as groupName, d.name as deviceName
          FROM group_devices as gd
                INNER JOIN customer_groups as cg ON gd.customer_group = cg.id
                INNER JOIN devices as d ON d.id = gd.device
         WHERE gd.customer_group = '$groupid'
           AND gd.device = '$deviceid'
    SQL;
    $selectResult = mysqli_query($myconnect, $sqlSelect);
    $row = $selectResult->fetch_assoc();
    var_dump($row);
    return $row;
}
?>
