<?php

include '../include/db.php';

$secret_key="itsasecret"; // This would need to come from the database attached to the customer

$_POST=json_decode(file_get_contents('php://input'),true);

$customerid=$_POST["customerid"];
$groupname=$_POST["groupname"];
$secret=$_POST["secret"]; // sent in at register from frog

// temp for testing
// $customerid="e88c1675-9be1-40b0-88f2-9914a4036d0b";
// $groupname="Test Group 1";
// $secret="abcd1234";

dbconnect();

if( $secret_key == $secret){
    echo "success!" . PHP_EOL;
    echo "Adding group $groupname." . PHP_EOL;
    
    $group = create_group($myconnect, $customerid, $groupname); // error handle
    if (empty($group)){
        echo "Group was unable to be created. Please try again." . PHP_EOL; // ultimately, this should throw a 400 with  information back
    } else {
        $groupname = $group['name'];
	$groupid = $group['id'];
        echo "The group $groupname with id of '$groupid' was successfully created." . PHP_EOL;
    }
}else{
    echo "secrets do not match!" . PHP_EOL;
};

function create_group($myconnect, $customerid, $groupname){
    $actionDatetime = date("Y-m-d H:i:s");
    $sqlInsert = <<<SQL
        INSERT INTO customer_groups(name, customer_id)
        VALUE ('$groupname','$customerid')
    SQL;
    mysqli_query($myconnect, $sqlInsert);

    $sqlSelect = <<<SQL
        SELECT *
          FROM customer_groups
         WHERE name = '$groupname'
           AND customer_id = '$customerid'
    SQL;
    $selectResult = mysqli_query($myconnect, $sqlSelect);
    $row = $selectResult->fetch_assoc();
    return $row;
}
?>
