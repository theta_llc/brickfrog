<?php
/*
 * https://stackoverflow.com/questions/66986631/php-jwt-json-web-token-with-rsa-signature-without-library
 * https://stackoverflow.com/questions/65773502/how-to-generate-jwt-in-php
 * https://www.sitepoint.com/php-authorization-jwt-json-web-tokens/
 * https://jwt.io/introduction
 * https://dzone.com/articles/create-your-jwts-from-scratch
 */

$email = "s.dude@theta-llc.com";

generate_jwt($email);

function generate_jwt($email):String{
    $now = time();
    // 8 hours at a time, adjust to whatever makes sense later. 
    $expires = $now + 28800;

    $env = parse_ini_file('.env');
    $signing_key = $env['SIGNING_KEY'];

    $header = [ 
        "alg" => "HS512", 
        "typ" => "JWT" 
    ];
    $header = base64_url_encode(json_encode($header));
    $payload =  [
        "iss" => "theta-llc.com",
        "aud" => $email,
        "exp" => $expires,
        "iat" => $now,
    ];
    $payload = base64_url_encode(json_encode($payload));
    $sig_hash = hash_hmac('sha512', "$header.$payload", $signing_key, true);
    $signature = base64_url_encode($sig_hash);
    $jwt = "$header.$payload.$signature";
    echo $jwt . PHP_EOL;
    return $jwt;    
}

function base64_url_encode($text){
    return str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($text));
}

?>