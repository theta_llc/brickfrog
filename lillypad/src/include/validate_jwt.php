<?php


// $jwt = "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ0aGV0YS1sbGMuY29tIiwiYXVkIjoicy5kdWRlQHRoZXRhLWxsYy5jb20iLCJleHAiOjE2ODIwMjQzMDMsImlhdCI6MTY4MjAyNDAwM30.yx-o4pMLESWzXtCb0FvCXNR98_jpfxp2ZFq7BmbwGcIOJuNIomN0nvx2yCg8RTBUWPQXBtruYMF-SDTd0lEjrg";

// validate_jwt($jwt);

function validate_jwt($jwt) {
    $is_valid = false;
    $env = parse_ini_file('.env');
    $signing_key = $env['SIGNING_KEY'];
    $now = time();
    
    $jwt_sploded = explode(".", $jwt);
    
    $header = $jwt_sploded[0];
    $payload = $jwt_sploded[1];
    $signature = $jwt_sploded[2];

    $sig_hash_encoded = base64_url_encode(hash_hmac('sha512', "$header.$payload", $signing_key, true));

    $validated = $signature == $sig_hash_encoded;
    if ($validated  && validate_timestamps($now, $payload)) {
        echo "JWT signature verified." . PHP_EOL;
        $is_valid = true;
    } else {
        // probably want logging here. 
        echo "Bad signature or timestamp, abort!" . PHP_EOL;
    }
    
    return $is_valid;
}

function base64_url_encode($text){
    return str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($text));
}

/* Do we want to validate timestamps between issued and expired? 
 * What else do we want to validate, if anything, from the payload 
 * */
function validate_timestamps($now, $payload){
    $is_valid = false;

    $payload_decode = base64_decode($payload);
    echo $payload_decode . PHP_EOL;
    $payload_json = json_decode($payload_decode, true);
    $issued = $payload_json["iat"];
    $expires = $payload_json["exp"];
    
    if ($now > $issued && $now < $expires){
        echo "JWT timestamp is valid." . PHP_EOL;
        $is_valid = true;
    } else {
        // probably want logging here. 
        echo "JWT timestamp is not valid, abort!" . PHP_EOL;
    }
    return $is_valid;
}

?>