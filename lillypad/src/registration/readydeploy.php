<?php

function set_frog_rdp($myconnect, $deviceId, $service){
  echo "Readying frog to deploy" . PHP_EOL;

  $regionNotSet = empty(get_region($myconnect, $deviceId));
  $roleNotSet = empty(get_role($myconnect, $deviceId));
  if ($regionNotSet || $roleNotSet){
    echo "Frog not ready to deploy." . PHP_EOL;
  } else {
    $actionDatetime = date("Y-m-d H:i:s");
    $sql = <<<SQL
      UPDATE devices
        SET ready_to_deploy = 1, modified_by = '$service', modified = '$actionDatetime'
      WHERE id='$deviceId'
      AND is_registered<>0;
  SQL;
    $result = mysqli_query($myconnect, $sql); // returns `bool(true)` whether writes or  not... need error handling here somehow
    echo "ready result: $result" . PHP_EOL;
    var_dump($result);
  
    echo "Frog ready to deploy" . PHP_EOL;
  }
}

function get_role($myconnect, $deviceId){
  $sql = <<<SQL
    SELECT role_id
    FROM devices
    WHERE id = '$deviceId'
    AND is_registered = 1;
SQL;

  $result = mysqli_query($myconnect, $sql);
  $row = $result->fetch_assoc();
  return $row['role_id'];;
}

function get_region($myconnect, $deviceId){
  $sql = <<<SQL
    SELECT region_id
    FROM devices
    WHERE id = '$deviceId'
    AND is_registered = 1;
SQL;

  $result = mysqli_query($myconnect, $sql);
  $row = $result->fetch_assoc();
  return $row['region_id'];
}

?>
