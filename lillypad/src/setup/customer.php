<?php

include '../include/db.php';

$secret_key="itsasecret"; // This would need to come from the database attach>
$json=json_decode($_GET["json"],true);
$header=$json["header"];
$package=$json["data"];

$customer=$package["customer"];
$secret=$header["auth"];

// temp for testing
// $customer="Grumpy Co.";
// $secret="itsasecret";

dbconnect();

if( $secret_key == $secret){
    $customer = setup_customer($myconnect, $customer); // error handle
    if (empty($customer)){
        echo "Whoops! Error pulling up customer groups. Please try again." . PHP_EOL;
    } else {
        $id = $customer["id"];
        echo "Customer setup with id of: '$id'" . PHP_EOL;
    }
    return $customer;
}else{
    echo "secrets do not match!" . PHP_EOL;
};

function setup_customer($myconnect, $customer){
    $iddata = file_get_contents('/dev/urandom', false, NULL, 0, 16);
    assert($iddata !== false && $strong);
    $id = createuuid($iddata);
    $actionDatetime = date("Y-m-d H:i:s");
    $insert = <<<ISQL
        INSERT INTO `customer`(`id`, `name`, `created`, `created_by`)
        VALUES ('$id','$customer','$actionDatetime', 'custSetupService')
    ISQL;
    $insertResult = mysqli_query($myconnect, $insert);

    $select = <<<SSQL
        SELECT c.id
          FROM customer as c
         WHERE c.id = '$id';
    SSQL;
    $selectResult = mysqli_query($myconnect, $select);
    $row = $selectResult->fetch_assoc();
    return $row;
}

// https://stackoverflow.com/a/15875555/10358406
function createuuid($data){
    assert(strlen($data) == 16);

    $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}
?>