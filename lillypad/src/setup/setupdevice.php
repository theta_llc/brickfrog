frogname<?php

include '../include/db.php';

$secretkey="itsasecret"; // This would need to come from the database attached to the customer

$_POST=json_decode(file_get_contents('php://input'),true);

// $frogname=$_POST["frogname"]; // to be set later
$deviceid=$_POST["deviceid"]; // if null or empty, create? 
$customerid=$_POST["customerid"];
$secret=$_POST["secret"]; // sent in at register from frog
// $macaddress=$_POST["macaddress"]; // this will come in later

// initial testhost info
// $frogname="testhost";
// $deviceid="347f976f-d8a4-44fd-92a1-0aac08baf6f6";
// $customerid="e88c1675-9be1-40b0-88f2-9914a4036d0b";
// $secret="abcd1234";
// $macaddress='80:fa:5b:65:45:b6';

$setupService="setupService";

dbconnect();

if( $secretkey == $secret){
    echo "success!" . PHP_EOL;
    if( !empty( $deviceid) || !empty($customerid) ){
        echo "Setting up a frog." . PHP_EOL;
    }else{
        echo "theres no place like home!" . PHP_EOL;
        check_registration($myconnect,$deviceid);
    }
    $frog = setup_frog($myconnect, $deviceid, $customerid, $setupService); // error handle
    if (empty($frog)){
        echo "Frog was unable to be setup. Please try again." . PHP_EOL; // ultimately, this should throw a 400 with  information back
    } else {
        echo "Frog named was successfully setup";
    }
}else{
    echo "secrets do not match!" . PHP_EOL;
};

dbclose($myconnect);

function check_registration($myconnect, $deviceid){
    $sql = "SELECT name FROM devices WHERE id = '$deviceid'";
    $result = mysqli_query($myconnect, $sql);
    var_dump($result);
}

function setup_frog($myconnect, $deviceid, $customerid, $createdBy){
    $actionDatetime = date("Y-m-d H:i:s");
    $sqlInsert = <<<SQL
        INSERT INTO devices(id, customer_id, created, created_by)
        VALUES('$deviceid', '$customerid', '$actionDatetime', '$createdBy');
    SQL;
    $insertResult = mysqli_query($myconnect, $sqlInsert); // returns `bool(true)` whether writes or  not... need error handling here somehow

    // I tried doing these in one sql query and php mysqli got really angry. 
    $sqlSelect = <<<SQL
        SELECT d.id, d.customer_id
          FROM devices as d
         WHERE d.id = '$deviceid';
    SQL;
    $selectResult = mysqli_query($myconnect, $sqlSelect); // returns `bool(true)` whether writes or  not... need error handling here somehow
    $row = $selectResult->fetch_assoc();
    return $row;
}
