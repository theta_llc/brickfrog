<?php

include '../include/db.php';

$email=$_POST["email"];
$password=$_POST["password"];


// temp for testing
// $email="s.dude@theta-llc.com";
// $password="Password1234!";

dbconnect();

if( $email != null && $password != null){
    register_user($myconnect, $email, $password);
}

dbclose($myconnect);

function register_user($myconnect, $email, $password){
  $passwordHash = password_hash($password, PASSWORD_DEFAULT);
  if (password_verify($password, $passwordHash)) {
    $uuid = guidv4();
    $sql = <<<SQL
      INSERT INTO user (id, email, password)
      VALUES ('$uuid', '$email', '$passwordHash')
    SQL;
    mysqli_query($myconnect, $sql);

    return true;
  } 
  return false;
}

// https://www.uuidgenerator.net/dev-corner/php
function guidv4($data = null) {
    // Generate 16 bytes (128 bits) of random data or use the data passed into the function.
    $data = $data ?? random_bytes(16);
    assert(strlen($data) == 16);

    // Set version to 0100
    $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
    // Set bits 6-7 to 10
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80);

    // Output the 36 character UUID.
    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}

?>
