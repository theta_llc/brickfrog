<?php
// listener.php receives connections from "meters" and dutifully records
// what they send and the timestamp into a given file
if ($argc < 2) {
  echo "listener.php sits on port 7777, recording all messages and a timestamp.\n";
  echo "Usage: php listener.php <filename>\n";
  echo "<filename> will be created if not in existence, and appended to if it is.\n";
  exit();
}

$fileName = $argv[1];
$fp = fopen($fileName,'a');
$port = '7777';
$sock = socket_create_listen($port);

socket_getsockname($sock, $addr, $port);
print "Server Listening on $addr:$port\n";

while($retsock = socket_accept($sock)) {
   $what = socket_read($retsock,1024);
   if ($what=='quit') break;
   else fwrite($fp, $what.",".time()."\n");

   socket_getpeername($retsock, $raddr, $rport);
   print "Received Connection from $raddr:$rport\n";
}
fclose($fp);
socket_close($sock);

?>
