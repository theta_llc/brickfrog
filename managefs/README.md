# Brickfrog Go deploy file system for frogs when registered
## What is this?
This queiries a database view to get all the frogs that have recently been marked as "ready to deploy" and sets up the directory structure for them to be mounted. 
As the frogs are deployed fully three files, `package`, `manage`, and `log` will be placed into these directories signaling the frog is fully deployed. The program will then remove these directories from the file syste, and update the database accordingly. 

## Usage and Flags
Once compiled the command is as simple as `managefs {flag} {flag input}`

### Local Development
You will need to be connected to the company vpn in order to access the database, and you will need to obtain the `managefs.conf` file from a member of the team and place into the `/etc/frog.d/` directory. 

## Flags
* -l - String value of alternative base file path. Default "/srv/w/"
  * this is helpful for local testing and development


## Setting up the program and running it locally
1.  _**Prerequisites**_: 
    1. You will need to get a copy of a .env file from an internal developer in order to access the database
    2. This file will need to be in the 'managefs' directory, or the root fo wherever you will run the program from
2. initialize the project from the root directory
    ```
    go mod init brickfrog/managefs
    go mod tidy
    ```
3. build modules `dbconnect` and `dbservice` (`go build .` should finish without errors)
    1. cd into `dbconnect` and build 
        ```
        cd dbconnect
        go build .
        ```
    2. cd into `dbservice` and bulid
        ```
        cd dbservice // or cd `../dbservice` if you're still in dbconnect
        go build .
        ```
    3. cd into `deviceservice` and bulid
        ```
        cd deviceservice // or cd `../deviceservice` if you're still in dbservice
        go build .
        ```
4. cd back to root, install managefs
    ```
    go install brickfrog/managefs
    ```
5. you may need to add to your PATH in order to call the compiled program from BASH
    ```
    export PATH=$PATH:$(dirname $(go list -f '{{.Target}}' .))
    ```
6. run the program
    1. From local to test
        1. Command
            ```
            managefs -l './tmp/'
            ```
        2. wherever you ran this from you should see a new set of directories at `/tmp/`
        3. adding files `package`, `manage`, and `log` to the frog directories will update the table `rush.devices` and remove the directories from the file system
    2. Without flags
        1. Command
            ```
            managefs
            ```
        2. There is no output, and you will likely get an error if your user does not have write access to `/srv/w/`, otherwise that is where you will find the new directories 
7. Alternatively, you should be able to build the main project file and run with (this is how production would work as well)
    ```
    ./managefs {flag} {flag input}
    ```
8. Optional clean up, run `go clean -modcache` to clean up install cache