package dbconnect

import (
	"bufio"
	"database/sql"
	"log"
	"os"
	"strings"

	"github.com/go-sql-driver/mysql"
)

type Connection struct {
	User   string
	Passwd string
	Net    string
	Addr   string
	DBName string
}

func DbConnect() *sql.DB {
	// TODO: add URI and adjust code when we have a definitive answer to where and how to store this information
	connection := getConnection("/etc/frog.d/managefs.conf")

	cfg := mysql.Config{
		User:   connection.User,
		Passwd: connection.Passwd,
		Net:    connection.Net,
		Addr:   connection.Addr,
		DBName: connection.DBName,
	}

	db, err := sql.Open("mysql", cfg.FormatDSN())
	if err != nil {
		log.Fatal(err)
	}

	err = db.Ping()
	if err != nil {
		log.Fatal(err)
	}

	return db
}

func getConnection(configUri string) *Connection {
	f, err := os.Open(configUri)
	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	var connection = new(Connection)
	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		key, value, found := strings.Cut(scanner.Text(), "=")
		if !found {
			continue
		}

		// TODO: adjust format  when we define our config file
		switch key {
		case "USER":
			connection.User = value
		case "PASSWD":
			connection.Passwd = value
		case "NET":
			connection.Net = value
		case "SERVER":
			connection.Addr = value
		case "DB":
			connection.DBName = value
		default:
			log.Output(1, "Get Connection: Invalid Key in configuration file.")
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return connection
}
