package dbservice

import (
	"database/sql"
	"fmt"
	"time"
)

type DeviceToDeploy struct {
	Deviceid   string
	Customerid string
	Readyat    string
	Isdeployed bool
}

func GetDevicesToDeploy(db *sql.DB) ([]DeviceToDeploy, error) {
	var devices []DeviceToDeploy

	sql := `
		SELECT dtd.deviceid, dtd.customerid, dtd.readyat, dtd.isdeployed 
		FROM rush.devices_to_deploy as dtd;
	`
	rows, err := db.Query(sql)
	if err != nil {
		return nil, fmt.Errorf("SQL: %w", sql)
	}
	defer rows.Close()

	for rows.Next() {
		var device DeviceToDeploy
		if err := rows.Scan(&device.Deviceid, &device.Customerid, &device.Readyat, &device.Isdeployed); err != nil {
			return nil, err
		}
		devices = append(devices, device)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return devices, nil
}

func UpdateDeployedDevices(db *sql.DB, deviceid string) error {
	t := time.Now()
	deployed := fmt.Sprintf("%s", t.Format("2006-01-02 15:04:05"))
	sql := fmt.Sprintf("UPDATE rush.devices SET modified = '%s', modified_by = 'autocleanup', deployed = '%s'  WHERE id = '%s';", deployed, deployed, deviceid)
	_, err := db.Exec(sql)
	if err != nil {
		return err
	}
	return nil
}
