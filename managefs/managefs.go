package main

import (
	"flag"
	"log"

	"brickfrog/managefs/dbconnect"
	"brickfrog/managefs/dbservice"
	"brickfrog/managefs/manageservice"
)

func main() {
	loc := flag.String("l", "/srv/w/", "location for mounted directory")
	flag.Parse()
	baseFilePath := *loc
	db := dbconnect.DbConnect()
	for baseFilePath != "" {
		devicesToDeploy, err := dbservice.GetDevicesToDeploy(db)
		if err != nil {
			log.Fatal(err)
		}
	
		for _, device := range devicesToDeploy {
			go manageservice.MD(db, baseFilePath, device);
		}
	}
}
