package manageservice

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"

	"brickfrog/managefs/dbservice"
)

type Paths struct {
	Customer  string
	Device    string
	Package   string
	DateStamp string
	Manage    string
	Log       string
}

func MD(db *sql.DB, baseFilePath string, device dbservice.DeviceToDeploy) {
	paths, err := SetPaths(baseFilePath, device)
	if err != nil {
		log.Fatal(err)
	}

	if err := os.MkdirAll(paths.Device, 0770); err != nil {
		log.Fatal(err)
	}

	canDestroy :=hasfiles(paths)
	if canDestroy {
		if err := cleanUp(db, paths, device); err != nil {
			log.Fatal(err)
		}
	} else {
		if err := createDatetimeFile(paths); err != nil {
			log.Fatal(err)
		}
	}
}

func ManageDirectories(paths Paths, device dbservice.DeviceToDeploy) (bool, error) {
	if err := os.MkdirAll(paths.Device, 0770); err != nil {
		return false, err
	}

	hasfiles := hasfiles(paths)
	return hasfiles, nil
}

func SetPaths(baseFilePath string, device dbservice.DeviceToDeploy) (Paths, error) {
	var paths Paths
	paths.Customer = fmt.Sprintf("%s%s", baseFilePath, device.Customerid)
	paths.Device = fmt.Sprintf("%s/%s", paths.Customer, device.Deviceid)
	paths.Package = fmt.Sprintf("%s/package", paths.Device)
	paths.Manage = fmt.Sprintf("%s/manage", paths.Device)
	paths.Log = fmt.Sprintf("%s/log", paths.Device)
	datestamp, err := setDateStamp(device.Readyat)
	if err != nil {
		return paths, err
	}
	paths.DateStamp = fmt.Sprintf("%s/%s", paths.Device, datestamp)

	return paths, nil
}

func cleanUp(db *sql.DB, paths Paths, device dbservice.DeviceToDeploy) error {
	if err := dbservice.UpdateDeployedDevices(db, device.Deviceid); err != nil {
		return err
	} else {
		if err := os.RemoveAll(paths.Device); err != nil {
			return err
		}

		if err = removeParentIfEmpty(paths.Customer); err != nil {
			return err
		}
	}
	return nil
}

func createDatetimeFile(paths Paths) error {
	file, err := os.OpenFile(paths.DateStamp, os.O_RDONLY|os.O_CREATE, 0644)
	if err != nil {
		return err
	}
	defer file.Close()
	return nil
}

func setDateStamp(readyat string) (string, error) {
	readytime, err := time.Parse("2006-01-02 15:04:05", readyat)
	if err != nil {
		return "", err
	}
	return readytime.Format("060102150405"), nil
}

func hasfiles(paths Paths) bool {
	pkgfile, err := os.Open(paths.Package)
	if err != nil {
		return false
	}
	defer pkgfile.Close()

	mngfile, err := os.Open(paths.Manage)
	if err != nil {
		return false
	}
	defer mngfile.Close()

	logfile, err := os.Open(paths.Log)
	if err != nil {
		return false
	}
	defer logfile.Close()

	return true
}

func removeParentIfEmpty(path string) error {
	files, err := ioutil.ReadDir(path)
	if err != nil {
		return err
	}

	if len(files) != 0 {
		return nil
	}

	if err = os.Remove(path); err != nil {
		return err
	}

	return nil
}
