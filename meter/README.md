## Sensor simulator

This project contains a small sensor simulator running on port 7777 and a
listener that records the simulator's data.

Each sensor is a script that starts looping on

———>  until stop is triggered
|      wait a random amount of time
|	open a file
|	read in the number
|	increment by 1-3
|   send value to 7777
|	save the file
|	has stop been triggered?
———————|

The analysis script “observes” incoming messages from 7777

———>  until stop is triggered
|       did I get a message?
|	       yes - append Sensor ID (Filename) , power number, and timestamp to common file
|          no - continue
----------------|
