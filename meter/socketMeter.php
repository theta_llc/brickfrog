<?php
/*****

socketMeter.php is a Smart Meter simulator that periodically sends its file name and
current power value (in kW) to a listener.
- period is random to simulate latency
- file name is provided by the command line argument.
  - file exists, open file and read number to start with
  - file does not exist, create file and start eith '1'
- the power value starts with the number from the file (1 if no file) and:
    - increments over time
     - a real meter increments by one, but we adjust to simulate sending lag
     - time between increments changes based on simulated load
     - load experiences Spikes (higher load) and Valleys (lower load)
       - Spike causes shorter time between increments
       - Valley causes longer time between increments

*****/

$usage = "Usage: php socketMeter.php <sensor file> [opt - # iterations]
# interations is optional and will be 100 if not set.
For infinite iterations (run until kill), use 0.

socketMeter.php simulates a power meter. It reads a file for a start
value and increments in a way to simulate variations in power usage as
well as latency in transmissions.
The script triggers a socket send to port 7777 with the current sensor data.
";

if ($argc < 2) {
  exit($usage);
}

// check inputs
$arSend['sensorFile'] = $argv[1];
if (isset($argv[2]))  $numIterations = $argv[2];
else $numIterations = 10;
$numDone=0;

// socket information
$addr = '127.0.0.1';
$port = 7777 ;

$numToAdd = 1;
$timeMin = 1;
$timeMax = 3;
$eventEnd = 0;
$percentChanceSpike = 20;
$percentChanceValley = 32;

// default kW value
$arSend['kWValue'] = 1500;

// note... here is where we could include an optional config file
// possibly to overwrite default variable values

// if our file already exists, grab the value
// TRELLO: Change the client so that when it starts i can "read in" 
// a meter value from a text file.
if (file_exists($arSend['sensorFile'])) {
  echo "File <".$arSend['sensorFile']."> exists.\n";

  // intval resets to 0 if the value read in is not a numberic string
  $arSend['kWValue'] = intval(file_get_contents($arSend['sensorFile']));
  echo "Inital kW Value is <".$arSend['kWValue'].">\n";
}

// TRELLO: Change the client so that it runs until it is stopped
// use numIterations as 0 in usage for this effect.
while (++$numDone<=$numIterations || $numIterations==0) {

  $sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
  socket_connect($sock, $addr, $port);
  $toSend = json_encode($arSend);

  socket_write($sock, $toSend, strlen($toSend));
  socket_close($sock);

  // TRELLO: on each update it writes the last sent value to a text file.
  file_put_contents($arSend['sensorFile'], $arSend['kWValue']);

  // check if the event has ended
  if ($numDone >= $eventEnd)  {

    // what event has orccured?
    $eventNext = random_int(1, 100);

    // use percentages to determmine what event is next
    if ($eventNext < $percentChanceSpike) {
      print "eventNext is: ".$eventNext." - Spike";
      $addMin = 1;
      $addMax = 3;
    } else if ($eventNext > (100 - $percentChanceValley)) {
      print "eventNext is: ".$eventNext." - Valley";
      $addMin = 0;
      $addMax = 1;
    } else {
      print "eventNext is: ".$eventNext." - Normal time";
      $addMin = 0;
      $addMax = 2;
    }

    // How many cycles is the spike or valley
    $eventLength = random_int(3,7);
    $eventEnd = $numDone + $eventLength;
    print " event for ".$eventLength." cycles\n";
  }

  // how many increments for this cycle
  $numToAdd = random_int($addMin,$addMax);

  // wait from min to max seconds for next increment
  $waitAmount = random_int($timeMin,$timeMax);
  sleep($waitAmount);

  // meters increment by 1, but the sending and the incrementing
  // happen on different timers IRL, so this "hack" helps simulate that
  $arSend['kWValue'] += $numToAdd;

}

?>
